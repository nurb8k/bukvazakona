<?php

namespace App\Http\Controllers;

use App\Http\Resources\HomePageResource;
use App\Models\HomePage;
use Illuminate\Http\Request;
class HomePageController extends Controller
{

    /**
     * @OA\Get(
     *     path="/api/home",
     *     @OA\Response(response="200", description="Display a listing of the resource")
     * )
     */
    public function index()
    {
        // get only the first record
        return HomePageResource::collection(HomePage::all()->take(1));
    }
}
