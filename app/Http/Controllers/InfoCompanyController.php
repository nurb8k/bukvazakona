<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreInfoCompanyRequest;
use App\Http\Requests\UpdateInfoCompanyRequest;
use App\Models\InfoCompany;

class InfoCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreInfoCompanyRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreInfoCompanyRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InfoCompany  $infoCompany
     * @return \Illuminate\Http\Response
     */
    public function show(InfoCompany $infoCompany)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InfoCompany  $infoCompany
     * @return \Illuminate\Http\Response
     */
    public function edit(InfoCompany $infoCompany)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateInfoCompanyRequest  $request
     * @param  \App\Models\InfoCompany  $infoCompany
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateInfoCompanyRequest $request, InfoCompany $infoCompany)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InfoCompany  $infoCompany
     * @return \Illuminate\Http\Response
     */
    public function destroy(InfoCompany $infoCompany)
    {
        //
    }
}
