<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOurServiceRequest;
use App\Http\Requests\UpdateOurServiceRequest;
use App\Models\OurService;

class OurServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreOurServiceRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOurServiceRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OurService  $ourService
     * @return \Illuminate\Http\Response
     */
    public function show(OurService $ourService)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OurService  $ourService
     * @return \Illuminate\Http\Response
     */
    public function edit(OurService $ourService)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateOurServiceRequest  $request
     * @param  \App\Models\OurService  $ourService
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOurServiceRequest $request, OurService $ourService)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OurService  $ourService
     * @return \Illuminate\Http\Response
     */
    public function destroy(OurService $ourService)
    {
        //
    }
}
