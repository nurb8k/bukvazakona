<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreInfoPageRequest;
use App\Http\Requests\UpdateInfoPageRequest;
use App\Models\InfoPage;

class InfoPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreInfoPageRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreInfoPageRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InfoPage  $infoPage
     * @return \Illuminate\Http\Response
     */
    public function show(InfoPage $infoPage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InfoPage  $infoPage
     * @return \Illuminate\Http\Response
     */
    public function edit(InfoPage $infoPage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateInfoPageRequest  $request
     * @param  \App\Models\InfoPage  $infoPage
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateInfoPageRequest $request, InfoPage $infoPage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InfoPage  $infoPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(InfoPage $infoPage)
    {
        //
    }
}
