<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HomePageResource extends JsonResource
{
    /**
     * @SWG\Get(
     *      path="/api/home",
     *      summary="Главная страница",
     *      tags={"HomePage"},
     *      @SWG\Response(response=200, description="successful operation", @SWG\Schema(ref="#/definitions/HomePageResource")),
     *      @SWG\Response(response=400, description="Bad request"),
     *  )
     */
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'sub_title' => $this->sub_title,
            'description' => $this->description,
            'image' => $this->image,
        ];
    }
}
