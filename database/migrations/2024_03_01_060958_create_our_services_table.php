<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOurServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('our_services', function (Blueprint $table) {
            $table->id();

            $table->string('content1_title');
            $table->string('content1_description');
            $table->string('content1_image');

            $table->string('content2_title');
            $table->string('content2_description');
            $table->string('content2_image');

            $table->string('content3_title');
            $table->string('content3_description');
            $table->string('content3_image');

            $table->string('quote1');
            $table->string('quote2');
            $table->string('quote3');
            $table->string('quote4');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('our_services');
    }
}
